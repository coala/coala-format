import re

from coala_format.loader.coalaJsonLoader import coalaJsonLoader


class ResultReporter:
    """
    Every test report class is a sub class of ResultReporter
    """

    def __init__(self, loader: coalaJsonLoader, coala_format):
        self.loader = loader
        self.coala_format = coala_format
