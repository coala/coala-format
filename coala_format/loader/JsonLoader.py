from coala_format.loader.Loader import Loader


class JsonLoader(Loader):
    """
    Use to load data from json output
    """
