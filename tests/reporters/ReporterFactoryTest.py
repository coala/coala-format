import unittest

from coala_format.reporters.cli import cli
from coala_format.loader.coalaJsonLoader import coalaJsonLoader
from coala_format.reporters.ReporterFactory import ReporterFactory


class ReporterFactoryTest(unittest.TestCase):

    def setUp(self):
        """
        Set up parser
        """
        self.loader = coalaJsonLoader()
        self.parser = cli.create_parser()

    def test_get_report(self):
        args = self.parser.parse_args(['--checkstyle'])
        factory = ReporterFactory(self.loader, self.parser, '', args)
        check = '<coala_format.reporters.CheckstyleReporter.CheckstyleReporter'
        self.assertEqual(check, str(factory.get_reporter()).split(" ")[0])

        args = self.parser.parse_args(['--junit'])
        factory = ReporterFactory(self.loader, self.parser, '', args)
        check = '<coala_format.reporters.JunitReporter.JunitReporter'
        self.assertEqual(check, str(factory.get_reporter()).split(" ")[0])

        args = self.parser.parse_args(['--tap'])
        factory = ReporterFactory(self.loader, self.parser, '', args)
        check = '<coala_format.reporters.TapReporter.TapReporter'
        self.assertEqual(check, str(factory.get_reporter()).split(" ")[0])

        args = self.parser.parse_args(['--table'])
        factory = ReporterFactory(self.loader, self.parser, '', args)
        check = '<coala_format.reporters.HtmlReporter.HtmlReporter'
        self.assertEqual(check, str(factory.get_reporter()).split(" ")[0])


if __name__ == '__main__':
    unittest.main()
