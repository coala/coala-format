coala-format
============

coala-format holds a collection of useful utilities that are used
to read output from coala and convert it to other test formats.
Currently coala-format supports formats like TAP, JUnit and Checkstyle.
You can easily extend coala-format to your project by simply providing
a custom format-loader file.

`Get Involved <http://coala.io/#/getinvolved>`_
